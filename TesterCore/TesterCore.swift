//
//  File.swift
//  TesterCore
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import Foundation

public func double(value: Int) -> Int {
    return value * 2
}